import unittest
import lab3


class TestLab3(unittest.TestCase):
    def test_common_graph(self):
        edges = [("A", "B"),
                 ("B", "D"), ("B", "E"), ("B", "C"),
                 ("C", "F"),
                 ("F", "C"), ("F", "H"),
                 ("E", "F"), ("E", "B"), ("E", "G"),
                 ("G", "H"), ("G", "J"),
                 ("H", "K"),
                 ("K", "L"),
                 ("L", "J"),
                 ("J", "I"),
                 ("I", "G")
                 ]
        graph = lab3.build_graph(edges, mode='')
        t_graph = lab3.build_graph(edges, mode='t')

        self.assertEqual(lab3.find_comp(graph, t_graph), 5)

    def test_empty_graph(self):
        edges = []
        graph = lab3.build_graph(edges, mode='')
        t_graph = lab3.build_graph(edges, mode='t')

        self.assertEqual(lab3.find_comp(graph, t_graph), 0)

    def test_full_graph(self):
        edges = [("A", "B"), ("A", "C"), ("A", "D"),
                 ("B", "A"), ("B", "C"), ("B", "D"),
                 ("C", "A"), ("C", "B"), ("C", "D"),
                 ("D", "B"), ("D", "C"), ("D", "A")
                 ]
        graph = lab3.build_graph(edges, mode='')
        t_graph = lab3.build_graph(edges, mode='t')

        self.assertEqual(lab3.find_comp(graph, t_graph), 1)


if __name__ == '__main__':
    unittest.main()