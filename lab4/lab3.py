time_in = dict()
time_out = dict()


def dfs2(graph, start, nodes, visited, result):
    if start in nodes:
        nodes.remove(start)
    visited.add(start)
    result.add(start)
    for neighbour in graph[start]:
        if neighbour in visited:
            continue
        visited, result = dfs2(graph, neighbour, nodes, visited, result)
    return visited, result


def dfs1(graph, start, clock, nodes,  visited):
    if not visited:
        visited = set()
    clock += 1
    time_in[start] = clock
    if start in nodes:
        nodes.remove(start)
    visited.add(start)
    for neighbour in graph[start]:
        if neighbour in visited:
            continue
        clock = dfs1(graph, neighbour, clock, nodes, visited)
    clock += 1
    time_out[start] = clock
    return clock


def find_comp(graph, t_graph):
    nodes = list(graph.keys())
    comp = []
    visited = set()
    sorted_vertex = []
    result = set()
    clock = 0
    while nodes:
        clock = dfs1(graph, nodes.pop(), clock, nodes, None)
    if graph:
        timeline = list(time_out.items())
        timeline.sort(key=lambda i: i[1])
        for pair in timeline:
            sorted_vertex.append(pair[0])
    while sorted_vertex:
        visited, result = dfs2(t_graph, sorted_vertex.pop(),
                               sorted_vertex,
                               visited, result)
        comp.append(result)
        # print(result)
        result = set()
    time_out.clear()
    return len(comp)


def read_edges(file):
    f = open(file, mode='r')
    edges = list()
    for line in f:
        edges.append(line.split())
    return edges


def build_graph(edges, mode):
    graph = dict()
    f_ver = 0
    s_ver = 1
    if mode == 't':
        f_ver = 1
        s_ver = 0
    for edge in edges:
        if not edge[f_ver] in graph:
            graph[edge[f_ver]] = [edge[s_ver]]
        else:
            graph[edge[f_ver]].append(edge[s_ver])
        if not edge[s_ver] in graph:
            graph[edge[s_ver]] = []
    return graph


if __name__ == "__main__":
    edges = read_edges('graph.txt')
    graph = build_graph(edges, mode='')
    t_graph = build_graph(edges, mode='t')
    print(find_comp(graph, t_graph))
