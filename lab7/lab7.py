import math


class PriorityQueue:
    queue: list
    size: int
    vertex: list

    def __init__(self, graph: dict, prior: dict):
        self.queue = []
        self.vertex = []
        self.size = -1
        for v in graph:
            self.insert(prior[v], v)

    def __contains__(self, item):
        if item in self.queue:
            return True
        return False

    def __bool__(self):
        if self.queue:
            return True
        return False

    @staticmethod
    def _parent(child_ind: int):
        return int(child_ind / 2)

    @staticmethod
    def _left_child(parent_ind: int):
        return 2 * parent_ind

    @staticmethod
    def _right_child(parent_ind: int):
        return 2 * parent_ind + 1

    def _swap(self, ind1: int, ind2: int):
        self.queue[ind1], self.queue[ind2] = \
            self.queue[ind2], self.queue[ind1]
        self.vertex[ind1], self.vertex[ind2] = \
            self.vertex[ind2], self.vertex[ind1]

    def _shift_up(self, cur_ind: int):
        while cur_ind > 0 and \
                self.queue[self._parent(cur_ind)] > self.queue[cur_ind]:
            self._swap(self._parent(cur_ind), cur_ind)
            cur_ind = self._parent(cur_ind)

    def _shift_down(self, cur_ind: int):
        max_ind = cur_ind
        left = self._left_child(cur_ind)
        right = self._right_child(cur_ind)
        if left <= self.size and self.queue[left] < self.queue[max_ind]:
            max_ind = left
        if right <= self.size and self.queue[right] < self.queue[max_ind]:
            max_ind = right
        if cur_ind != max_ind:
            self._swap(max_ind, cur_ind)
            self._shift_down(max_ind)

    def insert(self, prior: float, vertex):
        self.queue.append(prior)
        self.vertex.append(vertex)
        self.size += 1
        self._shift_up(self.size)

    def extract_min(self):
        self._swap(0, self.size)
        self.queue.pop(self.size)
        result = self.vertex[self.size]
        self.size -= 1
        self._shift_down(0)
        return result

    def change_priority(self, cur_ver, prior: float):
        cur_ind = self.vertex.index(cur_ver)
        old_prior = self.queue[cur_ind]
        self.queue[cur_ind] = prior
        if prior < old_prior:
            self._shift_up(cur_ind)
        else:
            self._shift_down(cur_ind)


def dijkstra(graph, weight, start, q=PriorityQueue):
    dist = dict()
    if not graph or not weight:
        return 0
    for ver in graph:
        dist[ver] = math.inf
    dist[start] = 0
    queue = q(graph, dist)
    while queue:
        u = queue.extract_min()
        for v in graph[u]:
            if dist[v] > dist[u] + weight[u][v]:
                dist[v] = dist[u] + weight[u][v]
                queue.change_priority(v, dist[v])
    return dist


if __name__ == '__main__':
    graph = {'A': ['B', 'C', 'D'],
             'B': ['A', 'C', 'D'],
             'C': ['B', 'D', 'A'],
             'D': ['A', 'C', 'B']}
    weight = {'A': {'B': 4, 'C': 2, 'D': 4},
              'B': {'C': 3, 'D': 2, 'A': 3},
              'C': {'B': 1, 'D': 4, 'A': 5},
              'D': {'B': 1, 'C': 4, 'A': 5}
              }
    print(dijkstra(graph, weight, 'A'))
