import lab7
import unittest


class TestStringMethods(unittest.TestCase):

    def test_class_graph(self):
        graph = {'A': ['B', 'C'],
                 'B': ['C', 'D', 'E'],
                 'C': ['B', 'D', 'E'],
                 'D': [],
                 'E': ['D']}
        weight = {'A': {'B': 4, 'C': 2},
                  'B': {'C': 3, 'D': 2, 'E': 3},
                  'C': {'B': 1, 'D': 4, 'E': 5},
                  'D': {},
                  'E': {'D': 1}}
        result = {'A': 0, 'B': 3, 'C': 2, 'D': 5, 'E': 6}
        self.assertEqual(lab7.dijkstra(graph, weight, 'A'), result)

    def test_empty_graph(self):
        graph = {}
        weight = {'A': {'B': 4, 'C': 2},
                  'B': {'C': 3, 'D': 2, 'E': 3},
                  'C': {'B': 1, 'D': 4, 'E': 5},
                  'D': {},
                  'E': {'D': 1}}
        result = 0
        self.assertEqual(lab7.dijkstra(graph, weight, 'A'), result)

    def test_empty_weight(self):
        graph = {'A': ['B', 'C'],
                 'B': ['C', 'D', 'E'],
                 'C': ['B', 'D', 'E'],
                 'D': [],
                 'E': ['D']}
        weight = {}
        result = 0
        self.assertEqual(lab7.dijkstra(graph, weight, 'A'), result)

    def test_non_oriented(self):
        graph = {'A': ['B', 'C', 'D'],
                 'B': ['A', 'C', 'D'],
                 'C': ['B', 'D', 'A'],
                 'D': ['A', 'C', 'B']}
        weight = {'A': {'B': 4, 'C': 2, 'D': 4},
                  'B': {'C': 3, 'D': 2, 'A': 3},
                  'C': {'B': 1, 'D': 4, 'A': 5},
                  'D': {'B': 1, 'C': 4, 'A': 5}
                  }
        result = {'A': 0, 'B': 3, 'C': 2, 'D': 4}
        self.assertEqual(lab7.dijkstra(graph, weight, 'A'), result)


if __name__ == '__main__':
    unittest.main()
