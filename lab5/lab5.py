class Edge:
    first_ver = ''
    second_ver = ''
    weight = 0

    def __init__(self, f, s, w):
        self.first_ver = f
        self.second_ver = s
        self.weight = w


class SystemSet:
    parent = list()
    rank = list()

    def __init__(self, n):
        self.parent = list(range(0, n))
        self.rank = [0] * n

    def make_set(self, i):
        self.parent[i] = i

    def find(self, i):
        if i != self.parent[i]:
            self.parent[i] = self.find(self.parent[i])
            i = self.parent[i]
        return i

    def union(self, i, j):
        i_id = self.find(i)
        j_id = self.find(j)
        if i_id == j_id:
            return
        if self.rank[i_id] > self.rank[j_id]:
            self.parent[j_id] = i_id
        else:
            self.parent[i_id] = j_id
        if self.rank[i_id] == self.rank[j_id]:
            self.rank[j_id] += 1


def kraskal(vertex, edges):
    sets = SystemSet(len(vertex))
    score = 0
    count = 0
    edges.sort(key=lambda edge: edge.weight)
    for edge in edges:
        if sets.find(vertex[edge.first_ver]) == sets.find(vertex[edge.second_ver]):
            continue
        sets.union(vertex[edge.first_ver], vertex[edge.second_ver])
        score += edge.weight
        count += 1
        if count == len(vertex) - 1:
            break
    return score


vertex = {'A': 0, 'B': 1, 'C': 2, 'D': 3, 'E': 4, 'F': 5}
edges = list()
edges.append(Edge('A', 'B', 2))
edges.append(Edge('B', 'C', 5))
edges.append(Edge('B', 'D', 2))
edges.append(Edge('C', 'D', 4))
edges.append(Edge('D', 'F', 7))
edges.append(Edge('E', 'F', 8))
edges.append(Edge('C', 'E', 2))
edges.append(Edge('D', 'E', 3))
print(kraskal(vertex, edges))

